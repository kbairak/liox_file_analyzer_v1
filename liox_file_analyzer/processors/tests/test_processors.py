import mock

from unittest import TestCase

from ..constants import (
    ASIAN_LANGUAGES,

    UNIT_TYPE_FILES,
    UNIT_TYPE_WORDS,
    UNIT_TYPE_PAGES,

    WORDS_PER_MINUTE,
    WORDS_PER_ROW,
    WORDS_PER_SLIDE,
)
from ..processors import (
    count_asian_words,
    get_file_units,
)


class GetFileUnitsTests(TestCase):
    """ Exercises get_file_units functionalities """

    def test_unit_type_files(self):
        """ Veriries if unit_type = UNIT_TYPE_FILES """

        file_obj = mock.Mock()
        self.assertEqual(get_file_units(file_obj, UNIT_TYPE_FILES, 'en-en'), 1)

    def test_unit_type_words_doc_file(self):
        """
        Verifies if unit_type = UNIT_TYPE_WORDS and is_doc_file
        """
        file_obj = mock.Mock(words=0, extension='txt')
        self.assertEqual(get_file_units(file_obj, UNIT_TYPE_WORDS, 'en-en'), 0)

    def test_unit_type_words_slide(self):
        """
        Verifies if unit_type = UNIT_TYPE_WORDS and slide
        """
        file_obj = mock.Mock(words=0, extension='jpg', pages=5)
        self.assertEqual(
            get_file_units(file_obj, UNIT_TYPE_WORDS, 'en-en'),
            file_obj.pages * WORDS_PER_SLIDE
        )

    def test_unit_type_words_minutes(self):
        """
        Verifies if unit_type = UNIT_TYPE_WORDS and minutes
        """
        file_obj = mock.Mock(words=0, extension='jpg', pages=0, minutes=5)
        self.assertEqual(
            get_file_units(file_obj, UNIT_TYPE_WORDS, 'en-en'),
            file_obj.minutes * WORDS_PER_MINUTE
        )

    def test_unit_type_words_rows(self):
        """
        Verifies if unit_type = UNIT_TYPE_WORDS and rows
        """
        file_obj = mock.Mock(words=0, extension='jpg', pages=0, minutes=0, rows=5)
        self.assertEqual(
            get_file_units(file_obj, UNIT_TYPE_WORDS, 'en-en'),
            file_obj.rows * WORDS_PER_ROW
        )

        file_obj = mock.Mock(words=0, extension='jpg', pages=0, minutes=0, rows=0)
        self.assertEqual(get_file_units(file_obj, UNIT_TYPE_WORDS, 'en-en'), 0)

    def test_unit_type_words_asian(self):
        """
        Verifies if unit_type = UNIT_TYPE_WORDS and asian
        """
        file_obj = mock.Mock(words=5, characters=10)

        for lang in ASIAN_LANGUAGES:
            self.assertEqual(
                get_file_units(file_obj, UNIT_TYPE_WORDS, lang),
                count_asian_words(lang, file_obj.characters)
            )

    def test_unit_type_pages(self):
        """
        Verifies if unit_type = UNIT_TYPE_PAGES
        """
        file_obj = mock.Mock(pages=5)
        self.assertEqual(
            get_file_units(file_obj, UNIT_TYPE_PAGES, 'en-en'),
            file_obj.pages
        )
