import os

from bs4 import BeautifulSoup
from unittest import TestCase

from ..utils import (
    exclude_no_translate_attributed_tags,
)


class ExcludeNoTranslateAttributedTags(TestCase):
    """ Exercises functionality for utils.exclude_no_translate_attributed_tags """
    test_files_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test_files')

    def _get_file_content(self, filename):
        file_path = os.path.join(self.test_files_dir, filename)
        text = ''

        with open(file_path) as test_file:
            file_content = test_file.read()
            soup = BeautifulSoup(file_content)
            soup = exclude_no_translate_attributed_tags(soup)
            text = soup.get_text()

        return text.replace('\n', '')

    def test_with_no_translate_attrs(self):
        """
        Verifies that the function works with tags that
        are attributed translate="no" and does not have
        any child tags
        """
        self.assertEquals(self._get_file_content('no_translate.html'), 'ABC')
        self.assertEquals(self._get_file_content('no_translate.xml'), 'ABC')

    def test_with_no_translate_and_yes_translate_child(self):
        """
        Verifies that the function works with tags that
        are attributed translate="no" and does have
        any child tags. The behavior is that the function
        should still remove even the child tags even if said
        child tags are attributed with translate="yes"
        """
        self.assertEquals(self._get_file_content('no_translate_w_yes_translate.html'), 'ABC')
        self.assertEquals(self._get_file_content('no_translate_w_yes_translate.xml'), 'ABC')