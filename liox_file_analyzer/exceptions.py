# -*- coding: utf-8 -*-


class LioxFileAnalyzerException(Exception):
    pass


class FileExtensionNotSupportedException(LioxFileAnalyzerException):
    pass
