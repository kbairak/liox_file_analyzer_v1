===================================
 Lionbridge onDemand File Analyzer
===================================

This package is used by `Lionbridge onDemand`_ for some portions of file
analyzation. It is geared toward speed-and-ballpark rather than exact accuracy.
You likely will not benefit from this tool unless you are trying to emulate
onDemand analyzing for your own testing purposes.

Note that this package was a simple extraction of certain code from our main
application. A more refined package will be provided in the future.

.. _Lionbridge onDemand: https://ondemand.lionbridge.com


Installation
------------

Installing this package is a standard affair, but their are two things that
should be pointed out:

1. The FFVideo package requires some system packages to be installed. For
   information on this see https://pypi.python.org/pypi/FFVideo.
2. NLTK data is required for some parsing, so you need to download that if you
   have not already.

.. code-block:: bash

    $ pip install liox_file_analyzer
    $ python -m nltk.downloader punkt


Processors Usage
----------------

.. code-block:: python
	
    >>> from liox_file_analyzer.processors import *
    >>> process_video('/tmp/file.mov')
    >>> process_audio('/tmp/file.mp3')
    >>> process_doc('/tmp/file.docx')
    >>> process_slides('/tmp/file.pptx')
    >>> process_archive('/tmp/file.zip')


Extractors Usage
----------------

The extractors will return the text from a given document. Note that the
``get_text()`` function automatically identifies the file type by extension. It
will raise ``FileExtensionNotSupportedException`` if the file you specified is
not supported.

Supported file types at this time:

- .pdf
- .pptx

.. code-block:: python

    >>> from liox_file_analyzer.extractors import get_text
    >>> my_pptx_text = get_text('example.pptx')
    >>> my_pdf_text = get_text('example.pdf')


Running Tests
-------------

Via tox:

.. code-block:: bash

    $ pip install tox
    $ tox

Or directly with py.test:

.. code-block:: bash

    $ pip install -U pytest
    $ python -m nltk.downloader punkt
    $ py.test -s -v
